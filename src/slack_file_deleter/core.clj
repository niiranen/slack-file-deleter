(ns slack-file-deleter.core
  (:gen-class)
  (:require [clj-http.client :as client])
  (:require [clojure.string :as string])
  (:require [environ.core :refer [env]])
  (:require [clojure.tools.cli :refer [parse-opts]])
  (:gen-class)
  (:require [clj-time.core :as t])
  (:require [clj-time.coerce :as c])
  (:require [clj-time.format :as f])
  (:require [clojure.data.json :as json])
  (:use [slingshot.slingshot :only [throw+ try+]]))

(def file-types #{"all" "spaces" "snippets" "images" "gdocs" "zips" "pdfs"})

(def cli-options
  [[nil "--min-age DAYS" "Minimum age in days to delete"
    :default 60
    :parse-fn #(Integer/parseInt %)]
   [nil "--channel-id CHANNEL_ID" "What channel to filter by."
    :default ""]
   [nil "--type FILE_TYPE" "What file type to filter by."
    :default "all"
    :validate [#(contains? file-types %) (str "Must a valid file type, one of [" (string/join " " file-types) "]")]]
   [nil "--dry-run true/false" "If true, don't delete files, just list them."
    :default false
    :parse-fn #(Boolean/valueOf %)]
   ["-h" "--help"]])

(def fmt (f/formatters :date-time-no-ms))
(def basic-fmt (f/formatters :basic-date-time))

(def end-point (env :endpoint))
(def slack-api-token (env :slack-api-token))

(defn get-files
  "Get file"
  [min-age channel-id type page]
  (do
    (if (<= page 1) (println (format "Getting files of type %1$s from %2$s that are older than %3$s"
                                     type
                                     (if-not (string/blank? channel-id) channel-id "everywhere")
                                     (f/unparse fmt (c/from-epoch min-age)))))
    (println "Processing page" page))
  (let [body (json/read-str ((client/get
                              (str end-point "files.list?token=" slack-api-token "&ts_to=" min-age "&types=" type (if-not (string/blank? channel-id) (str "&channel=" channel-id)) "&page=" page)
                              {:accept :json})
                             :body)
                            :key-fn keyword)]
    (let [{:keys [files paging]} body]
      (if (< (paging :page) (paging :pages))
        (concat files (get-files min-age channel-id type (inc (paging :page))))
        files))))

(defn print-file-info
  "Print info about a single file"
  [file]
  (println "name:" (file :name) "size:" (file :size) "title:" (file :title) "uploaded:" (f/unparse fmt (c/from-epoch (file :created)))))

(defn delete-file
  "Delete a single file"
  [file]
  (let [response (json/read-str
                  ((try+
                    (client/post (str end-point "files.delete?token=" slack-api-token "&file=" (file :id)) {:key-fn keyword})
                    (catch [:status 429] {:keys [request-time headers body]}
                      (do
                        (println "Rate limited, retry in" 10 "seconds")
                        (Thread/sleep (* 10 1000))
                        (delete-file file)))) :body) :key-fn keyword)]
    (if (response :ok)
      (print-file-info file)
      (do
        (println "Failed to delete" (file :id))
        (println "\tError:\n\t" response)))))

(defn delete-files
  "Delete a collection of files"
  [files]
  (doseq [file files] (delete-file file)))

(defn get-options
  [options]
  (let [{:keys [min-age channel-id type dry-run]} options]
    {:min-age (c/to-epoch (t/minus (t/now) (t/days min-age)))
     :channel-id channel-id
     :type type
     :dry-run dry-run}))

(defn -main [& args]
  (def params (parse-opts args cli-options))
  (cond
    ((params :options) :help) (do
                                (println (string/join \newline ["New tools for a post-GDPR world"
                                                                ""
                                                                "Usage: slack-file-deleter [options]"
                                                                ""
                                                                "Options:"
                                                                (params :summary)]))
                                (System/exit 0))
    (params :errors) (let [{:keys [errors]} params]
                       (println (string/join "\n" errors))
                       (System/exit 127)))
  (let [{:keys [min-age channel-id type dry-run]} (get-options (params :options))]
    (let [files (get-files min-age channel-id type 1)]
      (if-not dry-run
        (if-not (empty? files)
          (do
            (delete-files files)
            (println "Deleted" (count files) "files")
            (println "Freed" (/ (apply + (map :size files)) (* 1024.0 1024.0)) "MB"))
          (println "No files found"))
        (do
          (println "Got" (count files) "files")
          (println "Total size:" (/ (apply + (map :size files)) (* 1024.0 1024.0)) "MB"))))))
