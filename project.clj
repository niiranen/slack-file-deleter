(defproject slack-file-deleter "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "MIT"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/tools.cli "0.3.7"]
                 [clj-http "3.9.0"]
                 [org.clojure/tools.nrepl "0.2.12"]
                 [cljfmt "0.5.7"]
                 [environ "1.1.0"]
                 [clj-time "0.14.4"]
                 [org.clojure/data.json "0.2.6"]
                 [slingshot "0.12.2"]]
  :main ^:skip-aot slack-file-deleter.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}
             :dev [:project/dev :profiles/dev]
             :test [:project/test :profiles/test]
             :profiles/dev  {}
             :profiles/test {}
             :project/dev {:source-paths ["src" "tool-src"]
                           :dependencies [[midje "1.6.3"]]
                           :plugins [[lein-auto "0.1.3"]]}
             :project/test {}}
  :plugins  [[cider/cider-nrepl "0.15.1"]
             [lein-environ "1.1.0"]])
